﻿# flow-activiti
流程（activiti）的使用和管理

### 初始数据
流程相关的表会在启动服务是初始化进去。

中文学习手册
http://www.mossle.com/docs/activiti/index.html

eclipse安装Activiti设计插件
打开eclipse软件，然后点击菜单栏的help选项，选择install New Software，然后点击add
在弹窗的Location栏目输入如下地址: http://activiti.org/designer/update/
然后点击下一步，一直到完成，然后重启eclipse

新增申请用户
http://127.0.0.1:7070/flowUser/saveOrUpdate?sysNo=test&userId=1&name=zhangsan&empCode=333

新增审批用户
http://127.0.0.1:7070/flowUser/saveOrUpdate?sysNo=test&userId=2&name=lisi&empCode=4444
http://127.0.0.1:7070/flowUser/saveOrUpdate?sysNo=test&userId=3&name=wangwu&empCode=55555
http://127.0.0.1:7070/flowUser/saveOrUpdate?sysNo=test&userId=4&name=zhaoliu&empCode=666666

启动普通流程
http://127.0.0.1:7070/bizApplySimple/start?sysNo=test&userId=1&content=启动普通流程

终止普通流程
http://127.0.0.1:7070/bizApplySimple/end?sysNo=test&userId=1&processInstanceId=53137


获取流程的审批任务
http://127.0.0.1:7070/flowTask/findTask?sysNo=test&userId=2&processKey=simpleProcess
processKey: 流程的key[不传默认获取所有]

审批通过普通流程
http://127.0.0.1:7070/bizApplySimple/check?sysNo=test&userId=2&taskId=12505&status=20
status: 状态[20审核通过、30审核不通过(驳回)]

审批驳回普通流程
http://127.0.0.1:7070/bizApplySimple/check?sysNo=test&userId=2&taskId=20014&status=30

获取流程图片
http://127.0.0.1:7070/bizApplySimple/img?sysNo=test&userId=2&processInstanceId=53137
