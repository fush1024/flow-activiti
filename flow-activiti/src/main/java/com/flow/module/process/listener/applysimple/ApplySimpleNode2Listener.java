package com.flow.module.process.listener.applysimple;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 领导审核监听器
 * @author anxpp.com
 * 2016年12月24日 下午12:10:01
 */
public class ApplySimpleNode2Listener implements TaskListener {
	
	private static final long serialVersionUID = 4285398130708457006L;
	private final static Logger LOGGER = LoggerFactory.getLogger(ApplySimpleNode2Listener.class);
	
	@Override
	public void notify(DelegateTask task) {
		//获取设置的参数
		String checkType = task.getVariable("checkType", String.class);
		
		LOGGER.info("节点2审核监听器 - 来源类型[" + checkType + "]...");
		
		/*//设置任务处理候选人
		FlowUserService flowUserService = FrameSpringBeanUtil.getBean(FlowUserService.class);
		List<String> candidateUsers = new ArrayList<String>();
		candidateUsers.add(flowUserService.getId("test", "3"));
		candidateUsers.add(flowUserService.getId("test", "4"));
		//task.addCandidateUsers(candidateUsers);
		
		//设置会签
		task.setVariable("joinUsers", candidateUsers);*/
	}
}
