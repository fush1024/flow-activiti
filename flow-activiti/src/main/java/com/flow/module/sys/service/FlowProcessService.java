package com.flow.module.sys.service;

import java.io.InputStream;
import java.util.Map;

import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;

import com.system.handle.model.ResponseFrame;

public interface FlowProcessService {
	
	/**
	 * 启动流程实例
	 * @param processKey
	 * @param params
	 * @return
	 */
	public ResponseFrame start(String processKey, Map<String, Object> params);
	
	/**
	 * 暂停流程实例
	 * @param processInstanceId
	 * @return
	 */
	public ResponseFrame suspend(String processInstanceId);

	/**
	 * 根据流程实例ID获取流程实例对象
	 * @param processInstanceId
	 * @return
	 */
	public ProcessInstance getByProcessInstanceId(String processInstanceId);
	
	/**
	 * 获取实例是否暂停
	 * @param processInstanceId
	 * @return
	 */
	public boolean isSuspend(String processInstanceId);
	/**
	 * 结束流程实例
	 * @param processInstanceId
	 * @return
	 */
	public ResponseFrame end(String processInstanceId, Map<String, Object> params);
	/**
	 * 根据流量实例ID获取流程定义对象
	 * @param processInstanceId
	 * @return
	 */
	public ProcessDefinition getProcDefByProcessInstanceId(String processInstanceId);
	/**
	 * 根据流程实例生成图片流
	 * @param processInstanceId
	 * @return
	 */
	public InputStream diagramImg(String processInstanceId);

	public ProcessDefinitionEntity getProcDefEntityByProcessInstanceId(String processInstanceId);
}
