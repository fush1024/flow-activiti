package com.flow.module.sys.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * flow_user实体
 * @author autoCode
 * @date 2017-12-29 10:12:18
 * @version V1.0.0
 */
@Alias("flowTask")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class FlowTask extends BaseEntity implements Serializable {
	//编码
	private String id;
	//名称
	private String name;
	//创建时间
	private Date createTime;
	//流程实例ID
	private String processInstanceId;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
}