package com.flow.module.sys.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * flow_user实体
 * @author autoCode
 * @date 2017-12-29 10:12:18
 * @version V1.0.0
 */
@Alias("flowUser")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class FlowUser extends BaseEntity implements Serializable {
	//编码
	private String id;
	//用户编码
	private String userId;
	//来源系统
	private String sysNo;
	//姓名
	private String name;
	//员工编码
	private String empCode;
	//创建时间
	private Date createTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSysNo() {
		return sysNo;
	}
	public void setSysNo(String sysNo) {
		this.sysNo = sysNo;
	}
}